﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M17_UF1_E1_MyVector_PuigmitjaLia
{
    internal class VectorGame
    {
        MyVector[] llistaVectors;

        public MyVector[] RandomVectors(int llargada)
        {
            llistaVectors = new MyVector[llargada];
            
            for(int i = 0; i < llargada; i++)
            {
                llistaVectors[i] = VectorRandom();
                
             }
                return llistaVectors;

        }

        public MyVector VectorRandom()
        {
            Random rnd = new Random();
            int[] num = new int[4];
            for (int i = 0; i < num.Length; i++)
            {
                num[i] = rnd.Next(-100, 101);
            }
            MyVector vectorX= new MyVector(num);
                return vectorX;
            }

        public void Visualitzar()
        {
            Console.WriteLine("Elements de la llisa:");
            for (int i = 0; i <= llistaVectors.Length - 1; i++)
            {
                if (llistaVectors[i] != null)
                {
                    Console.WriteLine(llistaVectors[i]);
                }
            }
        }
         

        public MyVector[] SortVectors(bool ordre)
        {
            switch (ordre)
            {
                case true:

                 
                        for (int i=0; i < llistaVectors.Length; i++)
                        {
                        for(int x=0; x < llistaVectors.Length - 1; x++)
                        {
                            if (llistaVectors[x].DistanciaVector() > llistaVectors[x + 1].DistanciaVector())
                        {
                            var aux= llistaVectors[x];
                            llistaVectors[x] = llistaVectors[x + 1];
                            llistaVectors[x + 1] = aux;
                        }
                        }
                     
                    }
                    Console.WriteLine("Ordre per Distancia:");
                    Visualitzar();
                    return llistaVectors;
                   

                case false:

                    for (int i = 0; i < llistaVectors.Length; i++)
                    {
                        for (int x = 0; x < llistaVectors.Length - 1; x++)
                        {
                            if (llistaVectors[x].DistanciaProximitat() > llistaVectors[x + 1].DistanciaProximitat())
                            {
                                var aux = llistaVectors[x];
                                llistaVectors[x] = llistaVectors[x + 1];
                                llistaVectors[x + 1] = aux;
                            }
                        }

                    }
                    Console.WriteLine("Ordre per Proximitat(0,0):");
                    Visualitzar();
                    return llistaVectors;
                
               
            }

            return llistaVectors;
        }



      



    }

}