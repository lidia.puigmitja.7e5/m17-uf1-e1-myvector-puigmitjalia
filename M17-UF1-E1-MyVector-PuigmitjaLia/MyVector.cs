﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_PuigmitjaLia
{
    internal class MyVector
    {
        int[] vector = new int[4];

        public MyVector(int primerX, int primerY, int ultimX, int ultimY)
        {
            vector =new int[] { primerX, primerY, ultimX, ultimY};
        }

        public MyVector(int[] vectorX)
        {
            vector = vectorX;
        }

        public void SetPrimerX(int nouPrimerX)
        {
            vector[0] = nouPrimerX;
        }

        public void SetPrimerY(int nouPrimerY)
        {
            vector[1] = nouPrimerY;
        }

        public void SetSegonX(int nouSegonX)
        {
            vector[2] = nouSegonX;
        }

        public void SetSegonY(int nouSegonY)
        {
            vector[3] = nouSegonY;
        }

        public void SetVector(int[] nouVector)
        {
            vector = nouVector;
        }

        public int[] GetVector()
        {
            return vector;
        }

        public int GetPrimerX()
        {
            return vector[0];
        }

        public int GetPrimerY()
        {
            return vector[1];
        }

        public int GetSegonX()
        {
            return vector[2];
        }

        public int GetSegonY()
        {
            return vector[3];
        }

        public void CanviDireccio()
        {
          var aux = (vector[0],vector[1]);
            (vector[0], vector[1]) = (vector[2], vector[3]);
            (vector[2], vector[3]) = aux;


        }

        public double DistanciaVector()
        {
           
            return Math.Abs(Math.Round(Math.Sqrt(Math.Pow(vector[2] - vector[0], 2) + Math.Pow(vector[3] - vector[1], 2)),2));
        }

        public double DistanciaProximitat()
        {
            return Math.Abs(Math.Round((((vector[2] - vector[0]) * (vector[1])) - ((vector[0]) * (vector[3] - vector[1])) / DistanciaVector()), 2));
        }


        public override string ToString()
        {
            return $"Vector que va de la coordenada ({vector[0]},{vector[1]}) a la coordenada ({vector[2]},{vector[3]}).";
        }


    }
}
