﻿using System;

namespace M17_UF1_E1_MyVector_PuigmitjaLia
{
    class MenuMyVector
    {
        internal class Program
        {
            public static void Main()
            {
                Inici();
            }

            public static void Inici()
            {
                bool fi;
                do
                {
                    MostrarMenu();
                    fi = TractarOpcio();
                } while (!fi);
            }

            internal static void MostrarMenu()
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("MENÚ:");
                Console.WriteLine("\n");
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("[1] Exercici 1: Introducció: Hello {name}");
                Console.WriteLine("[2] Exercici 2: Classe MyVector");
                Console.WriteLine("[3] Exercici 3: Classe VectorGame");
                Console.WriteLine("[0] FI.");
            }

            static bool TractarOpcio()
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Escriu la opció:");
                var opcio = Console.ReadLine();
                var final = false;

                switch (opcio)
                {
                    case "1":
                        HelloName.Hello(HelloName.Name("Escriu el teu nom:"));
                        Console.ReadKey();
                        break;

                    case "2":
                        MyVector vector1 = new MyVector(1, 2, 3, 4);
                        Console.WriteLine(vector1);
                        vector1.SetPrimerX(2);
                        vector1.SetPrimerY(1);
                        vector1.SetSegonX(4);
                        vector1.SetSegonY(3);
                        Console.WriteLine($"Coordenada A ({vector1.GetPrimerX()}, {vector1.GetPrimerY()}) i coordenada B ({vector1.GetSegonX()}, {vector1.GetSegonY()}).");
                        vector1.CanviDireccio();
                        Console.WriteLine(vector1);
                        Console.WriteLine($"La distancia del vector és: {vector1.DistanciaVector()}");
                        Console.ReadKey();
                        break;

                    case "3":
                        VectorGame list = new VectorGame();
                        list.RandomVectors(4);
                        list.Visualitzar();
                        list.SortVectors(true);
                        list.SortVectors(false);
                        Console.ReadKey();
                        break;

                    case "0":
                        final = true;
                        break;

                    default:
                        Console.WriteLine("Opció incorrecta!\n");
                        break;
                }

                return final;
            }

           

        }
    }
}
